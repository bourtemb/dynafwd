# DynaFwd

## Description

Simple Tango device server to test the dynamic forwarded attribute feature.  
It was created to test [cppTango#896](https://gitlab.com/tango-controls/cppTango/-/issues/896)

DynaFwd Tango device class has 2 forwarded attributes:   

- DynaFwdAttribute is a dynamic forwarded attribute created in add_dynamic_attributes() hook with root attribute name defined by root_attr_name device property
- PogoFwdAttr is a standard forwarded attribute created by Pogo.  
As any standard forwarded attribute created by Pogo the `__root_att` PogoFwdAttr attribute property must be set to point to the desired root attribute.

## How to compile?

### Using a traditional Makefile

Open DynaFwd.xmi with your version of Pogo and generate a Makefile

Then simply type `make` to compile the device server.

### Using CMake

Open DynaFwd.xmi with your version of Pogo and generate a CMakeLists.txt

Then you can use the following commands to compile the device server:

```
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make
```

